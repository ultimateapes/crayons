#!/usr/bin/python3

from setuptools import setup
import io
import os

# Package meta-data
NAME = ''
DESCRIPTION = ''
URL = ''
EMAIL = ''
AUTHOR = ''
REQUIRES_PYTHON = ''
VERSION = None

# Packages required
REQUIRED = [
]

here = os.path.abspath(os.path.dirname(__file__))

# Load the package's __version__.py
about = {}
if not VERSION:
    with open(os.path.join(here, NAME, '__version__.py')) as f:
        exec(f.read(), about)
else:
    about['__version__'] = VERSION